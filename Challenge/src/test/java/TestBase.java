import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.junit.Assert;

import static java.lang.Thread.sleep;

public class TestBase {

    private WebDriver driver;
    CotizarSeguro cotizarSeguro;

    @Before
    public void setUp() {
        cotizarSeguro = new CotizarSeguro(driver);
        driver = cotizarSeguro.chromeDriverConnection();
        cotizarSeguro.visit("https://www.mercantilandina.com.ar/seguros-personales/hogar/");
    }
    @After
    public void close(){
        driver.close();
    }

}
