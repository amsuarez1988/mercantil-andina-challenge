import org.junit.Assert;
import org.junit.Test;

import static java.lang.Thread.sleep;

public class TestCase extends TestBase {
    @Test
    public void testing() throws InterruptedException {
        cotizarSeguro.cotización();
        sleep (3000);
        cotizarSeguro.resultados();
        Boolean valor = cotizarSeguro.vericar();
        Assert.assertEquals(true,valor );
    }
}
