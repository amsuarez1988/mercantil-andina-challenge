import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static java.lang.Thread.sleep;

public class CotizarSeguro extends Base {

        By cotizarLocator = By.xpath("//*[@id='main']/div[1]/div/div/div/div[3]/a");
        By nombreyApellidoLocator = By.xpath("//*[@placeholder='Nombre y Apellido *']");
        By telefonoLocator = By.xpath("//*[@placeholder='Teléfono *']");
        By correoElectronicoLocator = By.xpath("//*[@placeholder='Correo electrónico *']");
        By tipoViviendaLocator = By.xpath("//*[@id='tipovivienda']/option[2]");
        By superficeLocator = By.xpath("//*[@id='superficie']/option[2]");
        By ubicacionLocator = By.xpath("//*[@id='ubicacion']/option[3]");
        By cotizarAhora = By.xpath("//*[@id='cotizador-submit']");
        By results = By.xpath("//*[@id=cotizador-result]/div/table/tbody/tr[*]");
        By chatOnline = By.xpath("//*[@id='wc-button']/div[3]");


    public CotizarSeguro (WebDriver driver){
        super(driver);
    }

    public void cotización() throws InterruptedException {
        click(cotizarLocator);
        type("Alejandro Suarez", nombreyApellidoLocator);
        type("381454534", telefonoLocator);
        type("ale@gmail.com", correoElectronicoLocator);
        click(tipoViviendaLocator);
        click(superficeLocator);
        click(ubicacionLocator);
        sleep (2000);
        click(cotizarAhora);

    }

    public void resultados(){
        findElements(results);
    }

    public Boolean vericar(){
        Boolean r = isDisplayed(chatOnline);
        return r;
    }
}
