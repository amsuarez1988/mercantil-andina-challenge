import io.appium.java_client.imagecomparison.OccurrenceMatchingResult;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

import static java.lang.Thread.sleep;

public class MyDriver {

    //Declare a static driver variable
    static WebDriver driver;
    static String baseURL;
    //Setup driver
    @BeforeClass
    public static void setUp(){
        System.setProperty("webdriver.chrome.driver", "c:\\Selenium\\chromedriver.exe");
        driver = new ChromeDriver();
        baseURL = "https://www.mercantilandina.com.ar/seguros-personales/hogar/";
        driver.manage().window().maximize();
    }
    @Test
    public void test() throws InterruptedException {
        String ExpectedTitle = "“Seguro de hogar - Mercantil andina";
        /*WebElement gmailLink;
        gmailLink = driver.findElement(By.xpath("(//*[@id='menu-item-30907']/a)[1]"));
        int xOffset = gmailLink.getRect().getX();
        int yOffset = gmailLink.getRect().getY();*/
        driver.get(baseURL);
        /*Actions actionProvider = new Actions(driver);
        sleep (5000);
        actionProvider.moveByOffset(xOffset, yOffset).build().perform();*/
        /*driver.findElement(By.xpath("(//*[@id='menu-item-30908']/a)[1]")).click();*/
        driver.findElement(By.xpath("//*[@id='main']/div[1]/div/div/div/div[3]/a")).click();
        driver.findElement(By.xpath("//*[@placeholder='Nombre y Apellido *']")).sendKeys("Alejandro Suarez");
        driver.findElement(By.xpath("//*[@placeholder='Teléfono *']")).sendKeys("3815743211");
        driver.findElement(By.xpath("//*[@placeholder='Correo electrónico *']")).sendKeys("ale@gmail.com");
        driver.findElement(By.xpath("//*[@id='tipovivienda']/option[2]")).click();
        driver.findElement(By.xpath("//*[@id='superficie']/option[2]")).click();
        driver.findElement(By.xpath("//*[@id='ubicacion']/option[3]")).click();
        sleep (5000);
        driver.findElement(By.xpath("//*[@id='cotizador-submit']")).click();
        List<WebElement> resul = driver.findElements(By.xpath("//*[@id=cotizador-result]/div/table/tbody/tr[*]"));
        String ActualTitle = resul.get(5).getAttribute("Title");
        Assert.assertEquals(ExpectedTitle, ActualTitle);
    }

    @AfterClass
    public static void quitDriver(){
        driver.close();
    }
}

